package com.example.snake;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.example.snake.assets.Textures;
import com.example.snake.fragments.EndActivity;
import com.example.snake.graphics.MyGLSurfaceView;

public class GameActivity extends AppCompatActivity {

    private MyGLSurfaceView mSurfaceView;

    private final ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if(result != null) {
            boolean newGame = result.getData().getBooleanExtra("newGame",false);
            if (!newGame) finish();
            else mSurfaceView.newGame();
        }
    });

    @Override
    public void onBackPressed() {finish(); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSurfaceView = new MyGLSurfaceView(this);
        mSurfaceView.setClipToOutline(false);

        setContentView(mSurfaceView);
        mSurfaceView.setCallback(this::endScreen);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSurfaceView != null) {
            mSurfaceView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSurfaceView != null) {
            mSurfaceView.onPause();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        Textures.reset();
        super.onDestroy();
    }

    private void endScreen(int points) {
        Intent i = new Intent(this, EndActivity.class);
        Bundle b = new Bundle();
        b.putInt("points", points);
        i.putExtras(b);
        mLauncher.launch(new Intent(i));
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}