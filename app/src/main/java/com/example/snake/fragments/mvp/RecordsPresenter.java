package com.example.snake.fragments.mvp;

import androidx.annotation.NonNull;

import com.example.snake.data.Record;
import com.example.snake.util.MyServerClient;
import com.example.snake.util.ServerClient;

import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class RecordsPresenter extends MvpPresenter<RecordsPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        ServerClient client = MyServerClient.createService(ServerClient.class);
        Call<List<Record>> call = client.getRecords();
        call.enqueue(new Callback<List<Record>>() {
            @Override
            public void onResponse(@NonNull Call<List<Record>> call, @NonNull Response<List<Record>> response) {
                getViewState().fillRecords(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Record>> call, @NonNull Throwable t) {

            }
        });
    }
}
