package com.example.snake.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.snake.GameActivity;
import com.example.snake.R;
import com.example.snake.util.SharedPref;

public class MenuFragment extends Fragment {
    private View mView;
    private boolean soundsOn;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);

        AppCompatButton newGameButton = mView.findViewById(R.id.button_menu_new_game);
        newGameButton.setOnClickListener(v -> startActivity(new Intent(getContext(), GameActivity.class)));

        FragmentTransaction ft = getParentFragmentManager().beginTransaction();
        Button recordsButton = mView.findViewById(R.id.button_menu_records);
        recordsButton.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new RecordsFragment())
                .addToBackStack(null).commit());

        ImageView soundIndicator = mView.findViewById(R.id.imageView_menu_sound);
        soundsOn = SharedPref.getSoundSettings(getContext());
        if(!soundsOn) soundIndicator.setImageResource(R.drawable.ic_baseline_volume_off_24);
        soundIndicator.setOnClickListener(v -> {
            soundsOn = !soundsOn;
            SharedPref.setSound(getContext(), soundsOn);
            if(soundsOn) soundIndicator.setImageResource(R.drawable.ic_baseline_volume_up_24);
            else soundIndicator.setImageResource(R.drawable.ic_baseline_volume_off_24);
        });

        return mView;
    }
}