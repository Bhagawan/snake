package com.example.snake.fragments.mvp;

import com.example.snake.data.Record;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface RecordsPresenterViewInterface extends MvpView {

    @SingleState
    void fillRecords(List<Record> records);
}
