package com.example.snake.fragments.mvp;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface EndPresenterViewInterface extends MvpView {

    @OneExecution
    void switchCards();

    @SingleState
    void fillCard(int points);

    @OneExecution
    void showError(byte code);

}
