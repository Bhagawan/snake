package com.example.snake.fragments;

import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.snake.R;
import com.example.snake.fragments.mvp.EndPresenter;
import com.example.snake.fragments.mvp.EndPresenterViewInterface;
import com.example.snake.util.SharedPref;

import moxy.MvpAppCompatActivity;
import moxy.presenter.InjectPresenter;

public class EndActivity extends MvpAppCompatActivity implements EndPresenterViewInterface {

    @InjectPresenter
    EndPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);
        fullscreen();

        Intent i = getIntent();
        if(i != null) {
            int points = i.getExtras().getInt("points");
            mPresenter.initialize(points);
        } else finish();

        AppCompatButton newGameButton = findViewById(R.id.button_new_game);
        newGameButton.setOnClickListener(v -> exit(true));
        AppCompatButton exitButton = findViewById(R.id.button_end_exit);
        exitButton.setOnClickListener(v -> exit(false));

        AppCompatButton saveButton = findViewById(R.id.button_end_save);
        saveButton.setOnClickListener(v -> switchCards());

        AppCompatButton cancelSaveButton = findViewById(R.id.button_save_cancel);
        cancelSaveButton.setOnClickListener(v -> switchCards());
        AppCompatButton acceptSaveButton = findViewById(R.id.button_save_accept);
        acceptSaveButton.setOnClickListener(v -> {
            EditText inputName = findViewById(R.id.editText_save_name);
            mPresenter.saveRecord(inputName.getText().toString());
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void switchCards() {
        CardView endCard = findViewById(R.id.card_end);
        CardView saveCard = findViewById(R.id.card_save);

        Animation showAnimation = AnimationUtils.loadAnimation(this, R.anim.show);
        Animation hideAnimation = AnimationUtils.loadAnimation(this, R.anim.hide);

        if(endCard.getVisibility() == View.VISIBLE) {
            hideAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) { }
                @Override public void onAnimationEnd(Animation animation) {
                    endCard.setVisibility(View.INVISIBLE);
                }
                @Override public void onAnimationRepeat(Animation animation) { }
            });
            endCard.startAnimation(hideAnimation);

            showAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) {
                    saveCard.setVisibility(View.VISIBLE);
                }
                @Override public void onAnimationEnd(Animation animation) { }
                @Override public void onAnimationRepeat(Animation animation) { }
            });
            saveCard.startAnimation(showAnimation);
        } else {
            hideAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) { }
                @Override public void onAnimationEnd(Animation animation) {
                    saveCard.setVisibility(View.INVISIBLE);
                }
                @Override public void onAnimationRepeat(Animation animation) { }
            });
            saveCard.startAnimation(hideAnimation);

            showAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override public void onAnimationStart(Animation animation) {
                    endCard.setVisibility(View.VISIBLE);
                }
                @Override public void onAnimationEnd(Animation animation) { }
                @Override public void onAnimationRepeat(Animation animation) { }
            });
            endCard.startAnimation(showAnimation);
        }
    }

    @Override
    public void fillCard(int points) {
        TextView scoreText = findViewById(R.id.text_end_points);
        TextView recordText = findViewById(R.id.text_end_record);
        scoreText.setText(String.valueOf(points));
        int record = SharedPref.getRecord(this);
        if(record == 0) {
            SharedPref.setRecord(this, points);
            recordText.setText(String.valueOf(points));
        } else recordText.setText(String.valueOf(record));
        if(points < record) {
            ImageView starOne = findViewById(R.id.image_end_points_one);
            ImageView starTwo = findViewById(R.id.image_end_points_two);
            starOne.setImageResource(R.drawable.ic_baseline_star_half_24);
            starTwo.setImageResource(R.drawable.ic_baseline_star_half_24);
        } else SharedPref.setRecord(this, points);
    }

    @Override
    public void showError(byte code) {
        switch (code) {
            case 0:
                Toast.makeText(this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this, getString(R.string.msg_save_name), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void exit(boolean newGame) {
        Intent i = new Intent();
        Bundle b = new Bundle();
        b.putBoolean("newGame", newGame);
        i.putExtras(b);
        setResult(RESULT_OK, i);
        finish();
    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}