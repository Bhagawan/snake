package com.example.snake.fragments.mvp;

import androidx.annotation.NonNull;

import com.example.snake.util.MyServerClient;
import com.example.snake.util.ServerClient;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class EndPresenter extends MvpPresenter<EndPresenterViewInterface> {
    private int points;

    public void initialize(int points) {
        this.points = points;
        getViewState().fillCard(points);
    }

    public void saveRecord(String name) {
        if(name.equals("")) {
            getViewState().showError((byte) 1);
        } else {
            ServerClient serverClient = MyServerClient.createService(ServerClient.class);
            Call<ResponseBody> call = serverClient.sendRecord(points, name);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    getViewState().switchCards();
                }
                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) { getViewState().showError((byte) 0); }
            });
        }
    }
}
