package com.example.snake.fragments.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snake.R;
import com.example.snake.data.Record;

import java.util.List;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {

    private List<Record> records;

    public RecordsAdapter(List<Record> data) {
        this.records = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_record, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Record record = records.get(position);
        holder.place.setText(String.valueOf(position + 1));
        holder.name.setText(record.getName());
        holder.points.setText(String.valueOf(record.getPoints()));
    }

    @Override
    public int getItemCount() {
        if(records == null) return 0;
        else return records.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView points;
        private final TextView place;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.text_record_item_number);
            name = itemView.findViewById(R.id.text_record_item_name);
            points = itemView.findViewById(R.id.text_record_item_points);
        }
    }
}
