package com.example.snake.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.snake.R;
import com.example.snake.data.Record;
import com.example.snake.fragments.adapter.RecordsAdapter;
import com.example.snake.fragments.mvp.RecordsPresenter;
import com.example.snake.fragments.mvp.RecordsPresenterViewInterface;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class RecordsFragment extends MvpAppCompatFragment implements RecordsPresenterViewInterface {
    private View mView;

    @InjectPresenter
    RecordsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_records, container, false);

        AppCompatButton backButton = mView.findViewById(R.id.button_records_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        return mView;
    }

    @Override
    public void fillRecords(List<Record> records) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_records);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new RecordsAdapter(records));
    }
}