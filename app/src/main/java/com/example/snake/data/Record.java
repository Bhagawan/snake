package com.example.snake.data;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class Record {

    @SerializedName("name")
    private String name;
    @SerializedName("points")
    private int points;

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }
}
