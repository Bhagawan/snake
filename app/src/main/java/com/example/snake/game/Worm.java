package com.example.snake.game;

import android.os.SystemClock;
import android.util.Log;

import com.example.snake.assets.Textures;
import com.example.snake.graphics.Quad;
import com.example.snake.graphics.WormBody;
import com.example.snake.util.FloatPoint;

import java.util.ArrayList;
import java.util.List;

public class Worm {
    private static final double PERFECT_DT = 16.67 * 2;
    public static final byte UP = 0;
    public static final byte RIGHT = 1;
    public static final byte DOWN = 2;
    public static final byte LEFT = 3;
    private WormSegment tail;
    private WormSegment head;
    private int currSize, potentialSize;
    private byte direction = RIGHT;
    private float thickness = 1;
    private WormBody body;
    private Quad eye;
    private float crawlDistance = 0.01f;
    private float speed = 5; // square/second

    private float squareSize;

    public Worm(float x, float y, float squareSize) {
        this.squareSize = squareSize;

        tail = new WormSegment(x, y);
        head = new WormSegment(x + squareSize, y);
        tail.setNextPart(head);

        currSize = 1;
        potentialSize = 5;
        body = new WormBody();
        setThickness((float) (squareSize * 1.3));
        eye = new Quad(squareSize / 2, squareSize / 2, Textures.SNAKE_EYE);
        eye.setAngle(180);
    }

    public void update(float dT) {
        float speedMult = (float) (dT / PERFECT_DT);
        crawlDistance += (speed  * squareSize / 30) * speedMult;
        if(crawlDistance >= squareSize) {
            crawlDistance = 0.01f;
            crawl();
        } else {
            if(tail.getNextPart() != null && currSize == potentialSize) {
                tail.setX(tail.getNextPart().getX() + ((crawlDistance - squareSize) * Math.signum(tail.getNextPart().getX() - tail.getX())));
                tail.setY(tail.getNextPart().getY() + ((crawlDistance - squareSize) * Math.signum(tail.getNextPart().getY() - tail.getY())));
            }
            updateHead(tail);
        }
    }

    public void draw(float[] mVPMatrix) {
        body.setBody(tail);
        body.setOffset(2, 0);
        body.setColor(0,0, 0, 0.5f);
        body.draw(mVPMatrix);
        body.setOffset(0, 0);
        body.setColor(0.0f, 0.0f, 1.0f, 1.0f);
        body.draw(mVPMatrix);
        drawEyes(mVPMatrix);
    }

    private void drawEyes(float[] mVPMatrix) {
        switch (direction) {
            case UP:
                eye.setPosition(-head.getY(), head.getX() - (squareSize / 2));
                eye.draw(mVPMatrix);
                eye.setPosition(-head.getY(), head.getX() + (squareSize / 2));
                eye.draw(mVPMatrix);
                break;
            case DOWN:
                eye.setPosition(head.getY(), -head.getX() - (squareSize / 2));
                eye.draw(mVPMatrix);
                eye.setPosition(head.getY(), -head.getX() + (squareSize / 2));
                eye.draw(mVPMatrix);
                break;
            case RIGHT:
                eye.setPosition(-head.getX(), -head.getY() - (squareSize / 2));
                eye.draw(mVPMatrix);
                eye.setPosition(-head.getX(), -head.getY() + (squareSize / 2));
                eye.draw(mVPMatrix);
                break;
            case LEFT:
                eye.setPosition(head.getX(), head.getY() - (squareSize / 2));
                eye.draw(mVPMatrix);
                eye.setPosition(head.getX(), head.getY() + (squareSize / 2));
                eye.draw(mVPMatrix);
                break;
        }
    }

    public void crawl() {
        if(currSize == potentialSize) {
            addHead(tail);
            tail = tail.getNextPart();
        } else {
            addHead(tail);
            currSize++;
        }
    }

    public void setSpeed(float speed) {this.speed = speed;}

    public void setDirection(byte direction) {
        this.direction = direction;
        crawlDistance = 0.01f;
        crawl();
        switch (direction) {
            case UP:
                eye.setAngle(270);
                break;
            case RIGHT:
                eye.setAngle(180);
                break;
            case DOWN:
                eye.setAngle(90);
                break;
            case LEFT:
                eye.setAngle(0);
                break;
        }
    }

    public void growBy(int increment) {potentialSize += increment;}

    public void setThickness(float thickness) {
        this.thickness = thickness;
        body.setThickness(thickness);
    }

    public float getThickness() {
        return thickness;
    }

    public int getSize() {
        return potentialSize - 5;
    }

    public float getHeadX() {return head.getX();}

    public float getHeadY() {return head.getY();}

    public byte getDirection() {return direction;}

    public List<FloatPoint> getWormCoords() {
        return fillCoords(new ArrayList<>(), tail);
    }

    private ArrayList<FloatPoint> fillCoords(ArrayList<FloatPoint> coords, WormSegment wormSegment) {
        coords.add(new FloatPoint(wormSegment.getX(), wormSegment.getY()));
        if(wormSegment.getNextPart() == head) return coords;
        else return fillCoords(coords, wormSegment.getNextPart());
    }

    private void addHead(WormSegment wormSegment) {
        if(wormSegment.getNextPart() == head) {
            WormSegment newSegment = new WormSegment(wormSegment.getX(), wormSegment.getY());
            head.setX(wormSegment.getX());
            head.setY(wormSegment.getY());
            switch (direction) {
                case UP:
                    newSegment.setY(wormSegment.getY() + squareSize);
                    head.setY(newSegment.getY() + crawlDistance);
                    break;
                case RIGHT:
                    newSegment.setX(wormSegment.getX() + squareSize);
                    head.setX(newSegment.getX() + crawlDistance);
                    break;
                case DOWN:
                    newSegment.setY(wormSegment.getY() - squareSize);
                    head.setY(newSegment.getY() - crawlDistance);
                    break;
                case LEFT:
                    newSegment.setX(wormSegment.getX() - squareSize);
                    head.setX(newSegment.getX() - crawlDistance);
                    if(Math.abs(head.getY() - newSegment.getY()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    if(Math.abs(head.getX() - newSegment.getX()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    break;
            }
            wormSegment.setNextPart(newSegment);
            newSegment.setNextPart(head);
        } else addHead(wormSegment.getNextPart());
    }

    private void updateHead(WormSegment wormSegment) {
        if(wormSegment.getNextPart() == head) {
            switch (direction) {
                case UP:
                    head.setX(wormSegment.getX());
                    head.setY(wormSegment.getY() + crawlDistance);
                    if(Math.abs(head.getY() - wormSegment.getY()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    break;
                case RIGHT:
                    head.setX(wormSegment.getX() + crawlDistance);
                    head.setY(wormSegment.getY());
                    if(Math.abs(head.getX() - wormSegment.getX()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    break;
                case DOWN:
                    head.setX(wormSegment.getX());
                    head.setY(wormSegment.getY() - crawlDistance);
                    if(Math.abs(head.getY() - wormSegment.getY()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    break;
                case LEFT:
                    head.setX(wormSegment.getX() - crawlDistance);
                    head.setY(wormSegment.getY());
                    if(Math.abs(head.getX() - wormSegment.getX()) > squareSize) {
                        Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
                    }
                    break;
            }
        } else updateHead(wormSegment.getNextPart());
    }
}
