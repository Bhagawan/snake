package com.example.snake.game;

public class WormSegment {
    private float x, y;
    private WormSegment nextPart;

    public WormSegment(float x, float y) {
        this.x = x;
        this. y = y;
    }

    public WormSegment getNextPart() {
        return nextPart;
    }

    public void setNextPart(WormSegment nextPart) {
        this.nextPart = nextPart;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
