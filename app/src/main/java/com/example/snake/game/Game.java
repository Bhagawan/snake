package com.example.snake.game;

import com.example.snake.util.FloatPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game {
    private float mWidth, mHeight; // From the center of the screen.
    private float mTranslationX, mTranslationY;
    private float screenWidth, screenHeight;
    private GameCallback mCallback;
    private Field mField;
    private Worm worm;
    private Apple apple;
    private boolean gameOn = true;
    private boolean gameInitialized = false;
    private byte futureDir = -1;


    public Game() {}

    public void initialize(float screenWidth, float screenHeight, float gameWidth, float gameHeight) {
        this.mWidth = gameWidth;
        this.mHeight = gameHeight;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        mTranslationX = screenWidth / (mWidth * 2);
        mTranslationY = screenHeight / (mHeight * 2);
        mField = new Field(mWidth, mHeight);
        worm = new Worm(mField.fieldXtoScreenX((float)(mField.getWidth() / 2)),mField.fieldYtoScreenY((float)(mField.getHeight() / 2)),mField.getSquareSize());
        apple = new Apple((float) (mField.getSquareSize() * 1.4));
        moveApple();
        gameInitialized = true;
    }

    public void newGame() {
        worm = new Worm(mField.fieldXtoScreenX((float)(mField.getWidth() / 2)),mField.fieldYtoScreenY((float)(mField.getHeight() / 2)),mField.getSquareSize());
        moveApple();
        gameOn = true;
    }

    public void draw(float[] mVPMatrix, long dT) {
        //Log.d("worm_render 1 ", String.valueOf(SystemClock.uptimeMillis()));
        if(gameOn) {
            if(futureDir != -1) {
                worm.setDirection(futureDir);
                futureDir = -1;
            }
            worm.update(dT);
            checkWorm();
            checkApple();
        }
        mField.draw(mVPMatrix);
        apple.draw(mVPMatrix);
        worm.draw(mVPMatrix);
    }

    public void onTouch(float touchX, float touchY) {
        float headX = worm.getHeadX() * mTranslationX + screenWidth / 2;
        float headY = screenHeight / 2 - worm.getHeadY() * mTranslationY;

        switch (worm.getDirection()) {
            case Worm.DOWN:
            case Worm.UP:
                if(headX > touchX) futureDir = Worm.LEFT;
                else futureDir = Worm.RIGHT;
                break;
            case Worm.LEFT:
            case Worm.RIGHT:
                if(headY > touchY) futureDir = Worm.UP;
                else futureDir = Worm.DOWN;
                break;
        }
    }

    public void setCallback(GameCallback callback) {
        mCallback = callback;
    }

    public boolean isGameInitialized() {
        return gameInitialized;
    }

    private void moveApple() {
        ArrayList<FloatPoint> freeSquares = new ArrayList<>();
        List<FloatPoint> wormCoords = worm.getWormCoords();

        float screenX;
        float screenY;

        for(int y = 0; y < mField.getHeight(); y++) {
            screenY = mField.fieldYtoScreenY(y);
            for(int x = 0; x < mField.getWidth(); x++) {
                screenX = mField.fieldXtoScreenX(x);
                FloatPoint point = new FloatPoint(screenX, screenY);
                if(!wormCoords.contains(point)) freeSquares.add(point);
            }
        }
        apple.setPosition(freeSquares.get(new Random().nextInt(freeSquares.size())));
    }

    public interface GameCallback {
        void endGame(int size);
        void playSound();
    }

    private void checkApple() {
        List<FloatPoint> wormCoords = worm.getWormCoords();
        if(Math.abs(apple.getX() - wormCoords.get(wormCoords.size() - 1).x) <= 0.2 && Math.abs(apple.getY() - wormCoords.get(wormCoords.size() - 1).y) <= 0.2) {
            worm.growBy(1);
            if(mCallback != null) mCallback.playSound();
            moveApple();
        }
        if(Math.abs(apple.getX() - worm.getHeadX()) <= 0.2 && Math.abs(apple.getY() - worm.getHeadY()) <= 0.2) {
            worm.growBy(1);
            if(mCallback != null) mCallback.playSound();
            moveApple();
        }
    }

    private void checkWorm() {
        List<FloatPoint> wormCoords = worm.getWormCoords();
        for(FloatPoint point: wormCoords) {
            if(Collections.frequency(wormCoords, point) > 1 && gameOn) gameEnd();
        }
        if(Math.abs(worm.getHeadX()) > (mWidth ) || Math.abs(worm.getHeadY()) > mHeight) {
            gameEnd();
        }
    }

    private void gameEnd() {
        gameOn = false;
        if(mCallback != null) mCallback.endGame(worm.getSize());
    }
}
