package com.example.snake.game;

import com.example.snake.assets.Textures;
import com.example.snake.graphics.Quad;

public class Field {
    private static final byte FIELD_MIN_SQUARE_SIZE = 7;
    private static float squareSize;
    private int width, height;
    private float screenWidth, screenHeight;
    private Quad quad, border;

    public Field(float width, float height) {
        screenHeight = height * 2;
        screenWidth = width * 2;
        float min = Math.min(width, height) * 2;
        float amount = min / FIELD_MIN_SQUARE_SIZE;
        squareSize = FIELD_MIN_SQUARE_SIZE + (min % FIELD_MIN_SQUARE_SIZE) / amount;
        this.width = (int) (screenWidth / squareSize);
        this.height = (int) (screenHeight / squareSize);
        Textures.generateFieldBack(squareSize, this.width, this.height);
        quad = new Quad(squareSize * this.width, squareSize * this.height, Textures.FIELD_BACK);
        if(screenHeight > screenWidth) {
            float emptySpace = screenHeight - this.height * squareSize;
            border = new Quad(screenWidth, emptySpace, Textures.DARK_GREEN);
            border.setPosition( 0, (emptySpace - screenHeight) / 2);
            quad.setPosition(0, emptySpace / 2);
        } else {
            float emptySpace = screenWidth - this.width * squareSize;
            border = new Quad(emptySpace, screenHeight, Textures.DARK_GREEN);
            border.setPosition((screenWidth - emptySpace) / 2, 0);
            quad.setPosition(-(emptySpace / 2), 0);
        }
    }

    public void draw(float[] mVPMatrix) {
        quad.draw(mVPMatrix);
        border.draw(mVPMatrix);
    }

    public float getSquareSize() {
        return squareSize;
    }

    public float fieldXtoScreenX(float x) {
        return (squareSize / 2 + x * squareSize) - (screenWidth / 2);
    }

    public float fieldYtoScreenY(float y) {
        return (screenHeight / 2) - (squareSize / 2 + y * squareSize);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
