package com.example.snake.game;

import com.example.snake.assets.Textures;
import com.example.snake.graphics.Circle;
import com.example.snake.graphics.Quad;
import com.example.snake.util.FloatPoint;

public class Apple {
    private Quad apple;
    private Circle shadow;
    private float posX, posY;
    private float shadowoffsetY;

    public Apple(float size) {
        apple = new Quad(size, size, Textures.APPLE);
        shadowoffsetY = size / 20;
        shadow = new Circle(size / 2 - size / 10, 180);
        shadow.setColor(0.0f,0.0f, 0.0f, 0.5f);
    }

    public void draw(float[] mVPMatrix) {
        shadow.draw(mVPMatrix);
        apple.draw(mVPMatrix);
    }

    public void setPosition(float x, float y) {
        posX = x;
        posY = y;
        apple.setPosition(x, y);
        shadow.setPosition(x + 2, y);
    }

    public void setPosition(FloatPoint point) {
        posX = point.x;
        posY = point.y;
        apple.setPosition(point.x, point.y);
        shadow.setPosition(point.x + 2, point.y - shadowoffsetY);
    }

    public float getX() {return posX;}

    public float getY() {return posY;}
}
