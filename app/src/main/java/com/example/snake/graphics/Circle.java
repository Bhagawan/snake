package com.example.snake.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.snake.assets.Shaders;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Circle {
    private FloatBuffer vertexBuffer;

    private int vertexCount;
    private final int COORDS_PER_VERTEX = 3;
    private int vertexStride = COORDS_PER_VERTEX * 4;

    private float radius;
    private int count;

    private int positionHandle;
    private int colorHandle;
    float[] color = {1.0f, 1.0f, 1.0f, 1.0f};
    private int mProgram;
    private int mVPMatrixHandle;
    private final float[] transformMatrix = new float[16];
    private final float[] translateMatrix = new float[16];
    private final float[] scaleMatrix = new float[16];

    public Circle(float radius, int count) {
        this.radius = radius;
        this.count = count;

        ByteBuffer bb = ByteBuffer.allocateDirect((count + 2) * COORDS_PER_VERTEX * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        float[] coords = circleCoords();

        vertexBuffer.put(coords);
        vertexBuffer.position(0);

        mProgram = Shaders.simpleProgram;
        setScale(1 , 1);
        setPosition(0,0);
    }

    private float[] circleCoords() {
        vertexCount = count + 2;
        float[] coords = new float[vertexCount * COORDS_PER_VERTEX];
        int offset = 0;
        coords[offset++] = 0;
        coords[offset++] = 0;
        coords[offset++] = 0;
        for (int i = 0; i < count + 1; i++) {
            float angleInRadians = ((float) i / (float) count) * ((float) Math.PI * 2f);
            coords[offset++] = radius * (float) Math.sin(angleInRadians);
            coords[offset++] = radius * (float) Math.cos(angleInRadians);
            coords[offset++] = 0;
        }
        return coords;
    }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(mProgram);

        Matrix.setIdentityM(transformMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, mvpMatrix, 0, translateMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, transformMatrix, 0, scaleMatrix, 0);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);
        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);
        mVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, transformMatrix, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);

        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public void setColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }

    public void setScale(float width, float height) {
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix, 0, width, height, 1);
    }

    public void setPosition(float x, float y) {
        Matrix.setIdentityM(translateMatrix, 0);
        Matrix.translateM(translateMatrix, 0, x, y, 0);
    }

    public float getRadius() {
        return radius;
    }
}
