package com.example.snake.graphics;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;

import com.example.snake.assets.Shaders;
import com.example.snake.assets.Textures;
import com.example.snake.game.Game;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    private static final int VISIBILITY_RANGE = 100;
    private final float[] mVPMatrix = new float[16];
    private final float[] projectionMatrix = new float[16];
    private final float[] viewMatrix = new float[16];

    private float mHeight;
    private float mWidth;
    private long mTime = -1;

    private Game game;
    private Context context;

    public MyGLRenderer(Context context, Game game) {
        this.context = context;
        this.game = game;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        if(!Textures.isLoaded()) {
            Textures.load(context);
            Shaders.createShaders();
        }
    }

    public void onDrawFrame(GL10 unused) {
        long currTime = SystemClock.uptimeMillis();
        Matrix.setLookAtM(viewMatrix, 0, 0, 0, VISIBILITY_RANGE, 0.0f, 0.0f, -5.0f, 0f, 1.0f, 0.0f);
        Matrix.multiplyMM(mVPMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        if(mTime == -1) mTime = currTime;
        game.draw(mVPMatrix, currTime - mTime);
        mTime = currTime;
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        float ratio;
        if(height > width) {
            ratio = (float) width / height;
            mHeight = VISIBILITY_RANGE;
            mWidth = mHeight * ratio;
            Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, 1, VISIBILITY_RANGE);
        } else {
            ratio = (float) height / width;
            mWidth = VISIBILITY_RANGE;
            mHeight = mWidth * ratio;
            Matrix.frustumM(projectionMatrix, 0, -1, 1, -ratio, ratio, 1, VISIBILITY_RANGE);
        }
        if(!game.isGameInitialized()) game.initialize(width, height, mWidth, mHeight);
    }
}