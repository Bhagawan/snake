package com.example.snake.graphics;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.example.snake.assets.Shaders;
import com.example.snake.game.WormSegment;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

public class WormBody {
    private FloatBuffer vertexBuffer;
    private static int mProgram;
    private float thickness = 1;

    private int positionHandle;
    private int colorHandle;

    private int vPMatrixHandle;
    private static final int COORDS_PER_VERTEX = 3;

    private ArrayList<float[]> segments;

    private final float[] transformMatrix = new float[16];
    private final float[] offsetMatrix = new float[16];
    private static final int vertexStride = COORDS_PER_VERTEX * 4;
    private final float[] color = { 0.0f, 0.0f, 1.0f, 1.0f };

    static {
        mProgram = Shaders.simpleProgram;
    }

    public WormBody() { }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(mProgram);

        Matrix.setIdentityM(transformMatrix, 0);
        Matrix.multiplyMM(transformMatrix, 0, mvpMatrix, 0, offsetMatrix, 0);

        positionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        colorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);


        vPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        //GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, transformMatrix, 0);

        int n = 0;
        for (float[] segment: segments) {
            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, n, segment.length / COORDS_PER_VERTEX);
            n += segment.length / COORDS_PER_VERTEX;
        }

        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public void setColor(float red, float green, float blue, float alpha) {
        color[0] = red;
        color[1] = green;
        color[2] = blue;
        color[3] = alpha;
    }

    public void setThickness(float thickness) {this.thickness = thickness / 2;}

    public void setBody(WormSegment tail) {
        int n = checkLength(tail,0);
        float th;
        if (n > 10) th = (float) (thickness * 0.5);
        else th = (10 - ((float)n / 2)) / 10 * thickness;
        segments = new ArrayList<>();
        if(tail.getNextPart() == null) {
            segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 3, th));
            segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 1, th));
        } else {
            if(tail.getX() == tail.getNextPart().getX()) {
                if(tail.getY() > tail.getNextPart().getY()) {
                    segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 0, th));
                } else segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 2, th));
            } else {
                if(tail.getX() > tail.getNextPart().getX()) {
                    segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 1, th));
                } else segments.add(generateEnd(tail.getX(), tail.getY(), (byte) 3, th));
            }
             generateBody(tail, n);
        }
        generateBuffer();
    }

    private void generateBuffer() {
        int coordAmount = 0;
        for(float[] segment: segments) {
            coordAmount += segment.length;
        }
        ByteBuffer bb = ByteBuffer.allocateDirect(coordAmount * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        for(float[] segment: segments) {
            vertexBuffer.put(segment);
        }
        vertexBuffer.position(0);
    }

    private void generateBody(WormSegment tail, float n) {
        float th;
        if (n > 10) th = (float) (thickness * 0.5);
        else th = (10 - (n / 2)) / 10 * thickness;
        if(tail.getNextPart() != null) {
            float nextTh;
            if (n - 1 > 10) nextTh = (float) (thickness * 0.5);
            else nextTh = (10 - ((n - 1) / 2)) / 10 * thickness;
            segments.add(generateBodyPart(tail.getX(), tail.getY(), tail.getNextPart().getX(), tail.getNextPart().getY(), th, nextTh));
            if(tail.getNextPart().getNextPart() == null) {
                if(tail.getX() == tail.getNextPart().getX()) {
                    if(tail.getY() > tail.getNextPart().getY()) {
                        segments.add(generateEnd(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 2, nextTh));
                    } else if(tail.getY() < tail.getNextPart().getY()) segments.add(generateEnd(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 0, nextTh));
                } else {
                    if(tail.getX() > tail.getNextPart().getX()) {
                        segments.add(generateEnd(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 3, nextTh));
                    } else if(tail.getX() < tail.getNextPart().getX()) segments.add(generateEnd(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 1, nextTh));
                }
            } else {
                if(tail.getX() > tail.getNextPart().getX()) {
                    if(tail.getNextPart().getY() > tail.getNextPart().getNextPart().getY()) {
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 3, nextTh));
                    } else if(tail.getNextPart().getY() < tail.getNextPart().getNextPart().getY()) {
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 2, nextTh));
                    }
                } else if (tail.getX() < tail.getNextPart().getX()){
                    if(tail.getNextPart().getY() > tail.getNextPart().getNextPart().getY()) {
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 0, nextTh));
                    } else if(tail.getNextPart().getY() < tail.getNextPart().getNextPart().getY()){
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 1, nextTh));
                    }
                } else if(tail.getY() > tail.getNextPart().getY()) {
                    if(tail.getNextPart().getX() > tail.getNextPart().getNextPart().getX()) {
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 1, nextTh));
                    } else if(tail.getNextPart().getX() < tail.getNextPart().getNextPart().getX()){
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 2, nextTh));
                    }
                } else if(tail.getY() < tail.getNextPart().getY()){
                    if(tail.getNextPart().getX() > tail.getNextPart().getNextPart().getX()) {
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 0, nextTh));
                    } else if(tail.getNextPart().getX() < tail.getNextPart().getNextPart().getX()){
                        segments.add(generateCorner(tail.getNextPart().getX(), tail.getNextPart().getY(), (byte) 3, nextTh));
                    }
                }
                generateBody(tail.getNextPart(), --n);
            }
        }
    }

    private float[] generateBodyPart(float fromX, float fromY, float toX, float toY, float fromThick, float toThick) {
        float[] vertexes = new float[4 * 3];
        int n = 0;
        if(fromX == toX) {
            vertexes[n++] = fromX - fromThick;
            vertexes[n++] = fromY;
            vertexes[n++] = 0;
            vertexes[n++] = fromX + fromThick;
            vertexes[n++] = fromY;
            vertexes[n++] = 0;
            vertexes[n++] = fromX + toThick;
            vertexes[n++] = toY;
            vertexes[n++] = 0;
            vertexes[n++] = fromX - toThick;
            vertexes[n++] = toY;
            vertexes[n++] = 0;
        } else {
            vertexes[n++] = fromX;
            vertexes[n++] = fromY - fromThick;
            vertexes[n++] = 0;
            vertexes[n++] = fromX;
            vertexes[n++] = fromY + fromThick;
            vertexes[n++] = 0;
            vertexes[n++] = toX;
            vertexes[n++] = fromY + toThick;
            vertexes[n++] = 0;
            vertexes[n++] = toX;
            vertexes[n++] = fromY - toThick;
            vertexes[n++] = 0;
        }
        return vertexes;
    }

    private float[] generateCorner(float x, float y, byte direction, float thick) {
        //0 - up right, 1 - bottom right, 2 - bottom left, 3 = up left
        float[] vertexes = new float[92 * 3];
        int n = 0;
        vertexes[n++] = x;
        vertexes[n++] = y;
        vertexes[n++] = 0;
        int angleFrom = 0;
        switch (direction) {
            case 1:
                angleFrom = 270;
                break;
            case 2:
                angleFrom = 180;
                break;
            case 3:
                angleFrom = 90;
                break;
        }
        for(int i = angleFrom; i < angleFrom + 91; i++) {
            double angle = ((i * Math.PI)/ 180);
            vertexes[n++] = (float) (x + thick * Math.cos(angle));
            vertexes[n++] = (float) (y + thick * Math.sin(angle));
            vertexes[n++] = (float) 0;
        }
        return vertexes;
    }

    private float[] generateEnd(float x, float y, byte direction, float thick) { //0 - up, 1 - right, 2 - down, 3 = left
        float[] vertexes = new float[182 * 3];
        int n = 0;
        vertexes[n++] = x;
        vertexes[n++] = y;
        vertexes[n++] = 0;
        int angleFrom = 0;
        switch (direction) {
            case 1:
                angleFrom = 270;
                break;
            case 2:
                angleFrom = 180;
                break;
            case 3:
                angleFrom = 90;
                break;
        }
        for(int i = angleFrom; i < angleFrom + 181; i++) {
            double angle = (i * Math.PI) / 180 ;
            vertexes[n++] =  x + thick * (float) Math.cos(angle);
            vertexes[n++] =  y + thick * (float) Math.sin(angle);
            vertexes[n++] = (float) 0;
        }
        return vertexes;
    }

    public void setOffset(float x, float y) {
        Matrix.setIdentityM(offsetMatrix, 0);
        Matrix.translateM(offsetMatrix, 0, x, y, 0);
    }

    private int checkLength(WormSegment tail, int l) {
        l++;
        if(tail.getNextPart() != null) return checkLength(tail.getNextPart(), l);
        else return l;
    }
}
