package com.example.snake.graphics;

import android.content.Context;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.example.snake.R;
import com.example.snake.game.Game;
import com.example.snake.util.SharedPref;

public class MyGLSurfaceView extends GLSurfaceView {

    private MyGLRenderer renderer;
    private Game game;
    private GameInterface callback;
    private MediaPlayer mediaPlayer;

    public MyGLSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        game = new Game();
        renderer = new MyGLRenderer(getContext(), game);
        setPreserveEGLContextOnPause(true);

        setRenderer(renderer);
        game.setCallback(new Game.GameCallback() {
            @Override
            public void endGame(int size) {
                if(callback != null) callback.onEnd(size);
            }

            @Override
            public void playSound() {
                if(SharedPref.getSoundSettings(context)) {
                    mediaPlayer = MediaPlayer.create(context, R.raw.bite);
                    mediaPlayer.start();
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();
        switch (action) {

            case MotionEvent.ACTION_UP:
                game.onTouch(event.getX(), event.getY());
                break;
        }
        return true;
    }

    @Override
    public void onPause() {
        if(mediaPlayer != null) mediaPlayer.release();
        super.onPause();
    }

    public void setCallback(GameInterface callback) {
        this.callback = callback;
    }

    public void newGame() { game.newGame();}

    public interface GameInterface {
        void onEnd(int size);
    }

}
