package com.example.snake.util;


import androidx.annotation.Nullable;

public class FloatPoint {
    public float x, y;

    public FloatPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj instanceof FloatPoint)) return false;
        else return x == ((FloatPoint) obj).x && y == ((FloatPoint) obj).y;
    }
}
