package com.example.snake.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    public static void setRecord(Context context, int score) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SavedData", Context.MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putInt("record", score);
        myEdit.apply();
    }

    public static int getRecord(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SavedData", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("record", 0);
    }

    public static void setSound(Context context, boolean soundOn) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SavedData", Context.MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putBoolean("sound", soundOn);
        myEdit.apply();
    }

    public static boolean getSoundSettings(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SavedData", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("sound", true);
    }
}
