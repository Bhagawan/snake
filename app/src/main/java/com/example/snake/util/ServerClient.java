package com.example.snake.util;

import com.example.snake.data.Record;
import com.example.snake.data.SplashResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServerClient {

    @FormUrlEncoded
    @POST("Snake/addScore.php")
    Call<ResponseBody> sendRecord( @Field("points") int points, @Field("name") String name);

    @GET("Snake/score.json")
    Call<List<Record>> getRecords();

    @FormUrlEncoded
    @POST("Snake/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}
