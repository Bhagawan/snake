package com.example.snake.assets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import androidx.appcompat.content.res.AppCompatResources;

import com.example.snake.R;
import com.example.snake.util.Util;

public class Textures {
    public static int SNAKE_EYE, FIELD_BACK, LIGHT_GREEN, DARK_GREEN, APPLE;
    public static int TEXTURE_0, TEXTURE_1, TEXTURE_2, TEXTURE_3, TEXTURE_4 ,TEXTURE_5;
    private static Bitmap background;

    public static int loadTexture(Context context, int resourceId) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            bitmap.recycle();
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    public static int loadTexture(Bitmap bitmap) {
        int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        if (textureHandle[0] != 0) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            if(bitmap != background) bitmap.recycle();
        }

        if (textureHandle[0] == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    private static boolean loaded = false;

    public static void load(Context context) {
        if (!loaded) {
            LIGHT_GREEN = loadTexture(Util.createColorBitmap(64,64,  context.getResources().getColor(R.color.field_light)));
            DARK_GREEN = loadTexture(Util.createColorBitmap(64,64,  context.getResources().getColor(R.color.field_dark)));
            SNAKE_EYE = loadTexture(Util.createEyeBitmap());
            Drawable apple = AppCompatResources.getDrawable(context,R.drawable.ic_apple);
            if(apple != null) APPLE = loadTexture(Util.svgToBitmap(apple, 64, 64));
            else APPLE =loadTexture(Util.createColorBitmap(64, 64, Color.RED));
            loaded = true;
        }
    }

    public static void generateFieldBack(float squareSize, float width, float height) {
        FIELD_BACK = loadTexture(Util.createFieldBitmap((int)squareSize, (int)width, (int)height));
    }

    public static boolean isLoaded() { return Textures.loaded;}

    public static void reset() {
        loaded = false;
    }

}
